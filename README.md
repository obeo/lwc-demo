# LWC Link Grid
## Deployment
### Deploy to Scratch Org
```
sfdx force:source:push -u <OrgAlias/Username>
```

###Deploy to Sandbox
```
sfdx force:source:deploy -u <OrgAlias/Username> -p force-app/main/default/
```

## Resources
### LWC
[Salesforce LWC Component Reference](https://developer.salesforce.com/docs/component-library/documentation/lwc)

### Aura Enabled Cacheable
[Aura Enabled Reference](https://developer.salesforce.com/docs/atlas.en-us.lightning.meta/lightning/controllers_server_apex_auraenabled_annotation.htm)

### API Decorator
[API Reference](https://developer.salesforce.com/docs/component-library/documentation/lwc/lwc.js_props_public)

### Wire Decorator
[Wire Reference](https://rajvakati.com/2019/02/01/lightning-web-components-wire-service-3/)

[Wire Adapter Reference](https://developer.salesforce.com/docs/component-library/documentation/lwc/reference_ui_api)

### Track Decorator
[Track Reference](https://developer.salesforce.com/docs/component-library/documentation/lwc/js_props_private_reactive)

### LWC Config
[Config Reference](https://developer.salesforce.com/docs/component-library/documentation/lwc/lwc.reference_configuration_tags)

### CSS
[LWC CSS Reference](https://developer.salesforce.com/docs/component-library/documentation/lwc/lwc.create_components_css)

### Loops
[Looping Data in HTML](https://rajvakati.com/2018/12/23/lightning-web-components-render-list-2/)

[Iterating Data](https://developer.salesforce.com/docs/component-library/documentation/lwc/reference_directives)
