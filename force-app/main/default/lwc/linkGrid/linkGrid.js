import { LightningElement, api, wire, track } from 'lwc';
import getLinkGrid from '@salesforce/apex/LinkGridSettings.getListSettings';
// import ACCOUNT_NAME from '@salesforce/schema/Account.Name';
// import ACCOUNT_STATUS from '@salesforce/schema/Account.Status';
// import { getRecord } from 'lightning/uiListApi';

export default class LinkGrid extends LightningElement {
    @api gridGroup;
    @api itemsPerRow;
    @api linkTarget;
    @api labelPosition;
    @api linkCSS;
    @api imageCSS;
    @api labelCSS;

    @track linkSettings

    @wire(getLinkGrid, { gridGroup: '$gridGroup' })
        setSettings({data, error}) {
            if (data) {
                console.log('data: ', data);
                this.linkSettings = data;
            } else if (error) {
                this.linkSettings = undefined;
                console.log('error: ', error);
            }
        }

    get defaultLabelCSS() {
        return 'label ' + this.labelPosition + ' slds-text-heading_medium'
    }

    get defaultContainerCSS() {
        return 'container slds-col slds-size_1-of-' + this.itemsPerRow ;
    }
}