public with sharing class LinkGridSettings {
    @AuraEnabled(cacheable=true)
	public static List<GridLinks__c> getListSettings(String gridGroup){
		return [SELECT ID,Grid_Name__c, Image_URL__c, Label__c, Link_URL__c,Sort_Order__c  
				FROM GridLinks__c
				WHERE Grid_Name__c = :gridGroup];
    }
}