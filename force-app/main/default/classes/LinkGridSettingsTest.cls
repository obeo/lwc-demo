@isTest
public class LinkGridSettingsTest {
    
    @isTest static void getSettings() {
        
        // Insert new setting
        GridLinks__c newSettings = new GridLinks__c(Name='Facebook', 
            										Grid_Name__c='TestGrid',  
                                             		Image_URL__c='https://image.flaticon.com/icons/svg/174/174848.svg',
                                                    Link_URL__c='http://www.Facebook.com',
                                                    Label__c='Facebook',
                                                    Sort_Order__c=1); 
        insert newSettings;
        
        // Retrieve new setting from Controller
       	String gridGroup = 'TestGrid';
       	List<GridLinks__c> mySettings = LinkGridSettings.getListSettings(gridGroup);
       	System.assertEquals('Facebook',mySettings[0].Label__c,'Wrong Value'); 
       
    }
    
}